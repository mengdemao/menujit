menujit
====



## 简介
> IOT设备管理桌面端

## 文件描述

+ build: 编译文件(临时文件)
+ cmake: cmake脚本
+ installer:安装包生成器
+ tool: 工具以及脚本
+ doc: 文档
+ lib: 三方库存放
+ inc: 头文件
+ src: 源文件
+ ui: 界面设计文件
+ res: 资源文件


## 架构描述

```mermaid
graph LR

subgraph 前端
    QT界面 --> boost进程间通信
end

boost进程间通信--> 协议解析
boost进程间通信--> 插件管理
    
subgraph 中端
    协议解析 --> 功能管理
    功能管理 --> 系统管理
    插件管理 --> 系统管理
end

subgraph 后端
    系统管理 --> Linux
    系统管理 --> Windows
    系统管理 --> Macos
end
```

### 界面管理
> QT绘制界面,并且与中端和后端进行沟通

### 协议解析
> MQTT协议解析与打包

### 功能管理
> 具体功能的实现与设备有关

### 插件管理
> 实现一种机制可以使用户可以二次开发(暂时实现不了)

### 系统管理
> 封装操作系统相关功能

## 改动记录